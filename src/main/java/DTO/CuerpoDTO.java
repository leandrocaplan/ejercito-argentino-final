package DTO;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * DTO que modeliza la tabla "cuerpos". Definido como Entity de Hibernate
 * @author Leandro
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "cuerpos")
public class CuerpoDTO {

    @Id
    @Column(name = "Codigo", unique = true, nullable = false)
    private Integer codigo;

    @Column(name = "Denominacion", unique = false, nullable = false)
    private String denominacion;

    public CuerpoDTO(Integer codigo, String denominacion) {
        this.codigo = codigo;
        this.denominacion = denominacion;
    }

    public CuerpoDTO() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    @Override
    public String toString() {
        return "Cuerpo{" + "codigo=" + codigo + ", denominacion=" + denominacion + '}';
    }

}
