/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * DTO que modeliza los soldados dentro de la tabla "militares.
 * Definido como Entity de Hibernate
 * @author Leandro
 */
@Entity
@DiscriminatorValue(value="Soldado") 
public class SoldadoDTO extends MilitarDTO {

    //Uso los códigos en lugar de la descripción
    @Column(name = "Cuerpo", unique = false, nullable = false)
    private Integer codCuerpo;
    @Column(name = "Compania", unique = false, nullable = false)
    private Integer codCompania;
    @Column(name = "Cuartel", unique = false, nullable = false)
    private Integer codCuartel;

    public SoldadoDTO(Integer codigo, 
            String tipo, 
            String password, 
            String nombre, 
            String apellido, 
            String graduacion,
            Integer codCompania,
            Integer codCuerpo, 
            Integer codCuartel ) {
        super(codigo, tipo, password, nombre, apellido, graduacion);
        this.codCuerpo = codCuerpo;
        this.codCompania = codCompania;
        this.codCuartel = codCuartel;
    }

    public SoldadoDTO(Integer codCuerpo, Integer codCompania, Integer codCuartel) {
        this.codCuerpo = codCuerpo;
        this.codCompania = codCompania;
        this.codCuartel = codCuartel;
    }

    public SoldadoDTO() {
    }

    public Integer getCodCuerpo() {
        return codCuerpo;
    }

    public void setCodCuerpo(Integer codCuerpo) {
        this.codCuerpo = codCuerpo;
    }

    public Integer getCodCompania() {
        return codCompania;
    }

    public void setCodCompania(Integer codCompania) {
        this.codCompania = codCompania;
    }

    public Integer getCodCuartel() {
        return codCuartel;
    }

    public void setCodCuartel(Integer codCuartel) {
        this.codCuartel = codCuartel;
    }

    @Override
    public String toString() {
        return "SoldadoDTO{" + "codigo=" + this.getCodigo() + ", tipo=" + this.getTipo() + ", password=" + this.getPassword() + ", nombre=" + this.getNombre() + ", apellido=" + this.getApellido() + ", graduacion=" + this.getGraduacion() + "codCuerpo=" + codCuerpo + ", codCompania=" + codCompania + ", codCuartel=" + codCuartel + '}';
    }


    
}
