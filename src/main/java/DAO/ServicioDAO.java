/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DTO.ServicioDTO;
import controlador.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import DTO.MilitarDTO;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Component;

/**
 * DAO utilizado para administrar los servicios
 * @author Leandro
 */
@Component
public class ServicioDAO {

     /**
     * Declaro un atributo de tipo ArrayList <ServicioDTO>, en el que cargaré el
     * contenido de la tabla "servicios"
     */
    private ArrayList<ServicioDTO> servicios;

    /**
     * Este método carga el contenido de la tabla "servicios" en el atributo
     * "servicios".
     */
    public void cargarServicios() {
        try {
//            ////JOptionhowMessageDialog(null, "Entro a cargarMilitares");
            this.servicios = new ArrayList<ServicioDTO>();

            // ////JOptionhowMessageDialog(null, query);
            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            Query query = session.createQuery("from ServicioDTO");
            this.servicios = (ArrayList) query.list();
            //JOptionPane.showMessageDialog(null, this.servicios);
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro un código y una descripcion, y las
     * inserta en un nuevo registro de la tabla "servicios", de no existir un
     * registro con el codigo ingresado. En el otro caso caso,se actualizará un
     * registro ya existente.
 
     * @param codigo
     * @param descripcion 
     */
    public void ingresarServicio(String codigo, String descripcion) {
        try {

            BasicConfigurator.configure();
            Session session = HibernateUtil.getSessionFactory().openSession();
            session.beginTransaction();
            ServicioDTO nuevoServicio = new ServicioDTO(Integer.parseInt(codigo), descripcion);
            //JOptionPane.showMessageDialog(null, nuevoServicio);
            session.saveOrUpdate(nuevoServicio);
            //JOptionPane.showMessageDialog(null, this.servicios);

            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Este método recibe por parámetro el código correspondiente al servicio a
     * dar de baja, y lo elimina de la tabla. No hay posibilidad de que se
     * ingrese un codigo inexistente en la tabla, ya que desde la vista HTML el
     * usuario selecciona desde un menú desplegable el servicio que quiere dar de
     * baja, donde solo puede seleccionar un servicio ya existente.
     *
     * @param codigo 
     */
    public void bajaServicio(String codigo) {
        BasicConfigurator.configure();
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        try {

            String hql = "delete from ServicioDTO where codigo=:codigo";
            Query query = session.createQuery(hql);
            query.setString("codigo", codigo);
            //JOptionPane.showMessageDialog(null, query.executeUpdate());
            query.executeUpdate();
            session.getTransaction().commit();
            session.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    /**
     * Getter del atributo "servicios"
     * @return 
     */
    public ArrayList<ServicioDTO> getServicios() {
        return servicios;
    }
}
