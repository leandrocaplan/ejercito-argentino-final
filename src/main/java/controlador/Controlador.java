/**
 * Esta clase corresponde al controlador dentro de la arquitectura MVC.
 * Este mismo recibe las peticiones de los archivos HTML correspondientes a la vista.
 * Asímismo, instancia y utiliza los DAO para transferirle la información necesitada por estos mismos.
 */
package controlador;

import DAO.ServicioDAO;
import DAO.CompaniaDAO;
import DAO.CuartelDAO;
import DAO.ServicioRealizadoDAO;
import DAO.CuerpoDAO;
import DAO.MilitarDAO;
import DAO.HibernateUtil;
import DTO.MilitarDTO;
import java.io.File;
import java.util.List;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import javax.swing.JOptionPane;
import DTO.ServicioRealizadoDTO;
import DTO.SoldadoDTO;

@Controller
public class Controlador {

    /**
     * En primer lugar, instancio todos los DAO utilizando la anotación
     *
     * @Autowired,una inyección de dependencias que me permite evitar la
     * necesidad de usar un constructor para hacerlo. Para esto, las mismas
     * deben estar definidas con la anotación @Component.
     */
    @Autowired
    private MilitarDAO militarDAO;

    @Autowired
    private CompaniaDAO companiaDAO;

    @Autowired
    private CuerpoDAO cuerpoDAO;

    @Autowired
    private CuartelDAO cuartelDAO;

    @Autowired
    private ServicioDAO servicioDAO;

    @Autowired
    private ServicioRealizadoDAO servicioRealizadoDAO;

    /**
     * En este atributo me guardo los datos del militar que realizó el login. En
     * un principio la inicializo en null
     */
    private MilitarDTO militarLogeado = null;

    /**
     * Este método recibe una petición GET, que me redirige al menú principal en
     * caso de que haya un militar logueado o en su defecto a la página de
     * login(index), en caso de que no lo haya
     *
     * @param model
     * @return
     */
    @GetMapping("/menuPrincipal")
    public String menuPrincipal(Model model) {
        if (this.militarLogeado == null) {
            return "index";
        }
        model.addAttribute("militar", this.militarLogeado);
        return "menuPrincipal";
    }

    /**
     * Este método recibe una peticion POST con el código y la contraseña del
     * militar que quiere loguearse. De ser exitoso el login, me dirigirá a la
     * vista del menú principal, mostrando la graduación, nombre y apellido del
     * militar ingresao, y mostrando un menú de opciones correspondiente al tipo
     * de militar que ingresó.
     *
     * @param codigo
     * @param password
     * @param model
     * @return
     */
    @PostMapping("/login")
    public String login(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "password") String password, Model model) {
        String pagina = "error";
        try {
            //JOptionPane.showMessageDialog(null, "Entro al controller");
            if (codigo.isEmpty() || password.isEmpty()) {
                String msgError = "Uno de los campos ingresados está vacio";
                model.addAttribute("error", msgError);
                return pagina;
            }
            this.militarLogeado = militarDAO.login(codigo);

            if (this.militarLogeado == null) {
                String msgError = "No se encontro el militar correspondiente";
                model.addAttribute("error", msgError);
                pagina = "error";
            } else if (!this.militarLogeado.getPassword().equals(password)) {
                String msgError = "La contraseña no es correcta";
                model.addAttribute("error", msgError);
                this.militarLogeado = null;
                pagina = "error";
            } else {
                //JOptionPane.showMessageDialog(null, "Entro al else del controller");
                //JOptionPane.showMessageDialog(null, militar.getNombre());
                //JOptionPane.showMessageDialog(null, militar.getApellido());
                //JOptionPane.showMessageDialog(null, militar.getGraduacion());
                model.addAttribute("militar", this.militarLogeado);

                pagina = "menuPrincipal";
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e);
            pagina = "error";
        }
        return pagina;

    }

    /**
     * Este método recibe una petición POST con la acción que desea realizar el
     * usuario, y me redirige hacia la vista correspondiente a esta misma.
     *
     * @param opcion
     * @param model
     * @return
     */
    @PostMapping("/opcionesMilitar")
    public String opcionesMilitar(@RequestParam(name = "opcion") String opcion, Model model) {

        String pagina;
        switch (opcion) {
            /**
             * Esta opción mostrará la lista de todos los militares. En el caso
             * que sean soldados, mostrará su companía, cuerpo, cuartel, y sus
             * servicios asociados. Para eso,carga todas las tablas de la base
             * de datos y se la pasa a la página mostrarMilitares.html En este
             * caso, la página correspondiente relacionará todos los datos de la
             * base entre sí.
             */
            case "mostrar_militares":
                militarDAO.cargarMilitares();
                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();
                servicioDAO.cargarServicios();
                servicioRealizadoDAO.cargarServiciosRealizados();

                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("companias", companiaDAO.getCompanias());
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());
                model.addAttribute("servicios", servicioDAO.getServicios());
                model.addAttribute("serviciosRealizados", servicioRealizadoDAO.getServiciosRealizados());
                pagina = "mostrarMilitares";
                break;

            /**
             * Esta opción cargará todos los datos de la tabla "companias" , y
             * se los pasará a la página mostrarCompanias.html, para que los
             * muestre.
             */
            case "mostrar_companias":

                companiaDAO.cargarCompanias();
                model.addAttribute("companias", companiaDAO.getCompanias());

                pagina = "mostrarCompanias";
                //vista.forward(request, response);
                break;

            /**
             * Esta opción cargará todos los datos de la tabla "cuerpos" , y se
             * los pasará a la página mostrarCuerpos.html, para que los muestre.
             */
            case "mostrar_cuerpos":

                cuerpoDAO.cargarCuerpos();
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());

                pagina = "mostrarCuerpos";
                break;
            /**
             * Esta opción cargará todos los datos de la tabla "cuarteles" , y
             * se los pasará a la página mostrarCuarteles.html, para que los
             * muestre.
             */
            case "mostrar_cuarteles":

                cuartelDAO.cargarCuarteles();
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                pagina = "mostrarCuarteles";
                break;

            /**
             * Esta opción cargará todos los datos de la tabla "servicios" , y
             * se los pasará a la página mostrarServicios.html, para que los
             * muestre.
             */
            case "mostrar_servicios":
                servicioDAO.cargarServicios();
                model.addAttribute("servicios", servicioDAO.getServicios());

                pagina = "mostrarServicios";

                break;
            /**
             * Esta opción simplemente me redirigirá a una página donde podré
             * seleccionar el tipo de militar que quiero ingresar.
             */
            case "alta_militar":

                pagina = "ingresarMilitar";
                break;

            /**
             * Esta opción solo es seleccionable por un suboficial. Me permitirá
             * dar de alta un soldado. Primero debe verificar que exista al
             * menos una compania, un cuerpo y un cuartel en la base de datos,
             * ya que un soldado debe tener sí o sí uno de estos elementos
             * asociados a el. De faltar alguno de ellos, me redirigirá a la
             * página de error. Si no falta ninguno, le pasará a la página
             * ingresarSoldado.html los datos de las tablas "companias",
             * "cuerpos" y "cuarteles", para que el usuario los seleccione de un
             * menú desplegable, ademas de pedir los demas campos
             * correspondientes a todos los militares
             * (código,contraseña,nombre,apellido y graduación).
             */
            case "alta_soldado":
                String faltante = "";
                boolean faltaElemento = false;

                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();

                if (companiaDAO.getCompanias().isEmpty()) {
                    faltaElemento = true;
                    faltante = "companias";
                } else if (cuerpoDAO.getCuerpos().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuerpos";
                } else if (cuartelDAO.getCuarteles().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuarteles";
                }
                if (!faltaElemento) {
                    model.addAttribute("companias", companiaDAO.getCompanias());
                    model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                    model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                    pagina = "ingresarSoldado";
                } else {
                    model.addAttribute("error", "No se puede dar de alta un soldado, ya que no hay " + faltante + " disponibles");
                    pagina = "error";
                }
                break;
            /**
             * Esta opción me redirigirá a la página ingresarCompania.html, que
             * me pedirá los datos para dar de alta o actualizar una companía.
             */
            case "alta_compania":
                pagina = "ingresarCompania";
                break;

            /**
             * Esta opción me redirigirá a la página ingresarCuerpo.html, que me
             * pedirá los datos para dar de alta o actualizar un cuerpo.
             */
            case "alta_cuerpo":
                pagina = "ingresarCuerpo";
                break;

            /**
             * Esta opción me redirigirá a la página ingresarCuartel.html, que
             * me pedirá los datos para dar de alta o actualizar un cuartel.
             */
            case "alta_cuartel":
                pagina = "ingresarCuartel";
                break;

            /**
             * Esta opción me redirigirá a la página ingresarServicio.html, que
             * me pedirá los datos para dar de alta o actualizar un servicio.
             */
            case "alta_servicio":
                pagina = "ingresarServicio";
                break;

            /**
             * Esta opción carga la lista de todos los militares, y se la pasa a
             * la página "bajaMilitar.html", donde el usuario podrá seleccionar
             * desde un menú desplegable el militar que quiere dar de baja.
             */
            case "baja_militar":
                militarDAO.cargarMilitares();
                model.addAttribute("militares", militarDAO.getMilitares());
                pagina = "bajaMilitar";
                break;

            /**
             * Esta opción solo el seleccionable por los suboficiales. Carga la
             * lista de todos los soldados, y se la pasa a la página
             * "bajaMilitar.html", donde el usuario podrá seleccionar desde un
             * menú desplegable el soldado que quiere dar de baja.
             */
            case "baja_soldado":
                militarDAO.cargarSoldados();
                model.addAttribute("militares", militarDAO.getSoldados());
                pagina = "bajaMilitar";
                break;

            /**
             * Esta opción carga la lista de todas las companias, y se la pasa a
             * la página "bajaCompania.html", donde el usuario podrá seleccionar
             * desde un menú desplegable la companía que quiere dar de baja.
             */
            case "baja_compania":
                companiaDAO.cargarCompanias();
                model.addAttribute("companias", companiaDAO.getCompanias());
                pagina = "bajaCompania";
                break;

            /**
             * Esta opción carga la lista de todos los cuerpos, y se la pasa a
             * la página "bajaCuerpo.html", donde el usuario podrá seleccionar
             * desde un menú desplegable el cuerpo que quiere dar de baja.
             */
            case "baja_cuerpo":
                cuerpoDAO.cargarCuerpos();
                model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                pagina = "bajaCuerpo";
                break;

            /**
             * Esta opción carga la lista de todos los cuarteles, y se la pasa a
             * la página "bajaCuartel.html", donde el usuario podrá seleccionar
             * desde un menú desplegable el cuartel que quiere dar de baja.
             */
            case "baja_cuartel":
                cuartelDAO.cargarCuarteles();
                model.addAttribute("cuarteles", cuartelDAO.getCuarteles());
                pagina = "bajaCuartel";
                break;

            /**
             * Esta opción carga la lista de todos los servicios, y se la pasa a
             * la página "bajaServicio.html", donde el usuario podrá seleccionar
             * desde un menú desplegable el servicio que quiere dar de baja.
             */
            case "baja_servicio":
                servicioDAO.cargarServicios();
                model.addAttribute("servicios", servicioDAO.getServicios());
                pagina = "bajaServicio";
                break;
            /**
             * Esta opcion carga la lista de todos los militares y los
             * servicios, d y se la pasa a la página "asignarServicio.html", de
             * modo que el usuario pueda, desde un menú desplegable, seleccionar
             * el soldado y el servicio que quiere asociar, introduciendo ademas
             * su fecha de realizacion.
             *
             */
            case "asignar_servicio":
                servicioDAO.cargarServicios();
                militarDAO.cargarMilitares();
                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("servicios", servicioDAO.getServicios());

                pagina = "asignarServicio";
                break;
            /**
             * Esta opción carga las tablas "militares", "servicios" y "serviciosRealizados", y se
             * la pasa a la página "desasignarServicio", que las relaciona entre sí, de modo tal que
             * el usuario vea, desde un menú desplegable, cual es el servicio asignado que quiere dar de baja.
             */
            case "desasignar_servicio":
                militarDAO.cargarMilitares();
                servicioDAO.cargarServicios();
                servicioRealizadoDAO.cargarServiciosRealizados();
                model.addAttribute("militares", militarDAO.getMilitares());
                model.addAttribute("servicios", servicioDAO.getServicios());
                model.addAttribute("serviciosRealizados", servicioRealizadoDAO.getServiciosRealizados());

                pagina = "desasignarServicio";
                break;
            /**
             * Esta opción borra los datos del militar logueado previamente, y me redirige
             * a la pagina de login ("index.html").
             */
            case "logout":
                this.militarLogeado = null;
                pagina = "index";
                break;
            default:
                model.addAttribute("error", "Opción invàlida");
                pagina = "error";
                break;

        }

        //JOptionhowMessageDialog(null, "Salgo del switch");
        return pagina;
    }

    /**
     * Este método recibe una petición POST, con el tipo de militar que yo
     * quiero ingresar, y me redirige hacia la página de alta correspondiente.
     * En el caso de que el tipo de militar elegido sea un soldado, le pasa a la
     * página el listado de companías, cuerpos y cuarteles, de modo que el
     * usuario pueda elegir estos desde un menú desplegable.
     *
     * @param tipo
     * @param model
     * @return
     */
    @PostMapping("/ingresarMilitar")
    public String ingresarMilitar(@RequestParam(name = "tipo") String tipo, Model model) {
        String pagina = null;
        switch (tipo) {
            case "Oficial":
                pagina = "ingresarOficial";
                break;
            case "Suboficial":
                pagina = "ingresarSuboficial";
                break;

            case "Soldado":
                String faltante = "";
                boolean faltaElemento = false;

                companiaDAO.cargarCompanias();
                cuerpoDAO.cargarCuerpos();
                cuartelDAO.cargarCuarteles();

                if (companiaDAO.getCompanias().isEmpty()) {
                    faltaElemento = true;
                    faltante = "companias";
                } else if (cuerpoDAO.getCuerpos().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuerpos";
                } else if (cuartelDAO.getCuarteles().isEmpty()) {
                    faltaElemento = true;
                    faltante = "cuarteles";
                }
                if (!faltaElemento) {
                    model.addAttribute("companias", companiaDAO.getCompanias());
                    model.addAttribute("cuerpos", cuerpoDAO.getCuerpos());
                    model.addAttribute("cuarteles", cuartelDAO.getCuarteles());

                    pagina = "ingresarSoldado";
                } else {
                    model.addAttribute("error", "No se puede dar de alta un soldado, ya que no hay " + faltante + " disponibles");
                    pagina = "error";
                }

                break;

            default:
                model.addAttribute("error", "Opcion invalida");
                break;
        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código y la actividad de la
     * companía que quiero dar de alta o actualizar, e invoca al metodo
     * ingresarCompania del companiaDAO para hacerlo.
     *
     * @param codigo
     * @param actividad
     * @param model
     * @return
     */
    @PostMapping("/ingresarCompania")
    public String ingresarCompania(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "actividad") String actividad,
            Model model) {
        if (codigo.isEmpty() || actividad.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        companiaDAO.ingresarCompania(codigo, actividad);
        return "altaExitosa";
    }

    /**
     * Este método recibe una petición POST con el código,nombre y ubicación del
     * cuartel que quiero dar de alta o actualizar, e invoca al metodo
     * ingresarCuartel de cuartelDAO para hacerlo.
     *
     * @param codigo
     * @param nombre
     * @param ubicacion
     * @param model
     * @return
     */
    @PostMapping("/ingresarCuartel")
    public String ingresarCuartel(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "nombre") String nombre, @RequestParam(name = "ubicacion") String ubicacion, Model model) {
        if (codigo.isEmpty() || nombre.isEmpty() || ubicacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        cuartelDAO.ingresarCuartel(codigo, nombre, ubicacion);
        return "altaExitosa";
    }

    /**
     * Este método recibe una petición POST con el código y denominacion del
     * cuerpo que quiero dar de alta o actualizar, e invoca al metodo
     * ingresarCuerpo de cuerpoDAO para hacerlo.
     *
     * @param codigo
     * @param denominacion
     * @param model
     * @return
     */
    @PostMapping("/ingresarCuerpo")
    public String ingresarCuerpo(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "denominacion") String denominacion, Model model) {
        if (codigo.isEmpty() || denominacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        cuerpoDAO.ingresarCuerpo(codigo, denominacion);
        return "altaExitosa";
    }

    /**
     * Este método recibe una petición POST con el código y descripcion del
     * servicio que quiero dar de alta o actualizar, e invoca al metodo
     * ingresarServicio de servicioDAO para hacerlo.
     */
    @PostMapping("/ingresarServicio")
    public String ingresarServicio(@RequestParam(name = "codigo") String codigo, @RequestParam(name = "descripcion") String descripcion, Model model) {
        if (codigo.isEmpty() || descripcion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        servicioDAO.ingresarServicio(codigo, descripcion);
        return "altaExitosa";
    }

    /**
     * Este método recibe una petición POST con los datos del oficial que quiero
     * dar de alta o actualizar, e invoca al metodo ingresarOficial de
     * militarDAO para hacerlo. Verifica ademas que el código de militar
     * ingresado no exista en un militar de otro tipo, ya que esto podría
     * corromper la integridad de la base de datos.
     *
     * @param codigo
     * @param password
     * @param nombre
     * @param apellido
     * @param graduacion
     * @param model
     * @return
     */
    @PostMapping("/ingresarOficial")
    public String ingresarOficial(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Oficial")) {
            militarDAO.ingresarOficial(codigo, password, nombre, apellido, graduacion);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un oficial");
            return "error";
        }
    }

    /**
     * Este método recibe una petición POST con los datos del suboficial que
     * quiero dar de alta o actualizar, e invoca al metodo ingresarSuboficial de
     * militarDAO para hacerlo. Verifica ademas que el código de militar
     * ingresado no exista en un militar de otro tipo, ya que esto podría
     * corromper la integridad de la base de datos.
     *
     * @param codigo
     * @param password
     * @param nombre
     * @param apellido
     * @param graduacion
     * @param model
     * @return
     */
    @PostMapping("/ingresarSuboficial")
    public String ingresarSuboficial(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Suboficial")) {
            militarDAO.ingresarSuboficial(codigo, password, nombre, apellido, graduacion);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un suboficial");
            return "error";
        }

    }

    /**
     * Este método recibe una petición POST con los datos del soldado que quiero
     * dar de alta o actualizar, e invoca al metodo ingresarSoldado de
     * militarDAO para hacerlo. Verifica ademas que el código de militar
     * ingresado no exista en un militar de otro tipo, ya que esto podría
     * corromper la integridad de la base de datos.
     */
    @PostMapping("/ingresarSoldado")
    public String ingresarSoldado(@RequestParam(name = "codigo") String codigo,
            @RequestParam(name = "password") String password,
            @RequestParam(name = "nombre") String nombre,
            @RequestParam(name = "apellido") String apellido,
            @RequestParam(name = "graduacion") String graduacion,
            @RequestParam(name = "codCompania") String codCompania,
            @RequestParam(name = "codCuerpo") String codCuerpo,
            @RequestParam(name = "codCuartel") String codCuartel,
            Model model) {
        if (codigo.isEmpty() || password.isEmpty() || nombre.isEmpty() || apellido.isEmpty() || graduacion.isEmpty()) {
            String msgError = "Uno de los campos ingresados está vacío";
            model.addAttribute("error", msgError);
            return "error";
        }
        if (militarDAO.verificarTipo(codigo, "Soldado")) {
            militarDAO.ingresarSoldado(codigo, password, nombre, apellido, graduacion, codCompania, codCuerpo, codCuartel);
            return "altaExitosa";
        } else {
            model.addAttribute("error", "El militar ingresado existe y no corresponde a un soldado");
            return "error";
        }
    }

    /**
     * Este método recibe una petición POST con el código de militar a dar de
     * baja. Verifica que el mismo no tenga ningún servicio asignado, ya que de
     * ser así, al darse de baja podría corromper la integridad de la base de
     * datos, por lo que antes habrá que desasignarlos.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/bajaMilitar")
    public String bajaMilitar(@RequestParam(name = "militarBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;

            servicioRealizadoDAO.cargarServiciosRealizados();

            for (ServicioRealizadoDTO sr : servicioRealizadoDAO.getServiciosRealizados()) {
                //JOptionPane.showMessageDialog(null, sr);
                if (sr.getCodSoldado().toString().equals(codigo)) {
                    //JOptionPane.showMessageDialog(null, "Entro al if");
                    cargado = true;
                }
            }

            if (!cargado) {
                militarDAO.bajaMilitar(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el soldado, ya que el mismo tiene asignado un servicio");

                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";
        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código de compania a dar de
     * baja. Verifica que la misma no esté asignada a ningún soldado, ya que de
     * ser así, al darse de baja podría corromper la integridad de la base de
     * datos.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/bajaCompania")
    public String bajaCompania(@RequestParam(name = "companiaBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCompania().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                companiaDAO.bajaCompania(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja la compania, ya que la misma está asignada a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código del cuerpo a dar de
     * baja. Verifica que el mismo no esté asignado a ningún soldado, ya que de
     * ser así, al darse de baja podría corromper la integridad de la base de
     * datos.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/bajaCuerpo")
    public String bajaCuerpo(@RequestParam(name = "cuerpoBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCuerpo().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                cuerpoDAO.bajaCuerpo(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el cuerpo, ya que el mismo está asignado a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código del cuartel a dar de
     * baja. Verifica que el mismo no esté asignada a ningún soldado, ya que de
     * ser así, al darse de baja podría corromper la integridad de la base de
     * datos.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/bajaCuartel")
    public String bajaCuartel(@RequestParam(name = "cuartelBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;
            militarDAO.cargarMilitares();

            for (MilitarDTO militar : militarDAO.getMilitares()) {
                //JOptionPane.showMessageDialog(null, "Entro al for");
                if (militar instanceof SoldadoDTO) {
                    //JOptionPane.showMessageDialog(null, militar);
                    SoldadoDTO soldado = (SoldadoDTO) militar;
                    if (soldado.getCodCuartel().toString().equals(codigo)) {
                        cargado = true;
                    }
                }
            }

            if (!cargado) {
                cuartelDAO.bajaCuartel(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el cuartel, ya que el mismo está asignado a un soldado");
                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";

        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código de servicio a dar de
     * baja. Verifica que el mismo no esté asignado a ningún soldado, ya que de
     * ser así, al darse de baja podría corromper la integridad de la base de
     * datos.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/bajaServicio")
    public String bajaServicio(@RequestParam(name = "servicioBaja") String codigo, Model model) {
        String pagina = null;
        try {
            boolean cargado = false;

            servicioRealizadoDAO.cargarServiciosRealizados();

            for (ServicioRealizadoDTO sr : servicioRealizadoDAO.getServiciosRealizados()) {

                if (sr.getCodServicio().toString().equals(codigo)) {
                    cargado = true;
                }
            }

            if (!cargado) {
                servicioDAO.bajaServicio(codigo);
                pagina = "bajaExitosa";
            } else {
                model.addAttribute("error", "No se puede dar de baja el servicio, ya que está asignado a un soldado");

                pagina = "error";
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            model.addAttribute("error", e.getMessage());
            pagina = "error";
        }
        return pagina;
    }

    /**
     * Este método recibe una petición POST con el código de soldado, el código
     * de servicio, y la fecha de realización del servicio por parte del
     * soldado, y los asocia. Invoca al método asignarServicio de
     * servicioRealizadoDAO para hacerlo.
     *
     * @param codSoldado
     * @param codServicio
     * @param fecha
     * @param model
     * @return
     */
    @PostMapping("/asignarServicio")
    public String asignarServicio(@RequestParam(name = "codSoldado") String codSoldado,
            @RequestParam(name = "codServicio") String codServicio,
            @RequestParam(name = "fecha") String fecha,
            Model model) {
        servicioRealizadoDAO.asignarServicio(codSoldado, codServicio, fecha);
        return "altaExitosa";
    }

    /**
     * Este método recibe una petición POST con el código de servicio realizado
     * que quiero desasignar. El código de servicio realizado es
     * auto-incremental, por lo que es transparente para el usuario (el mismo
     * solo vé en la página el soldado, el servicio y la fecha asociadas entre
     * sí) Invoca para este propósito el método desasignarServicio de
     * servicioRealizadoDAO.
     *
     * @param codigo
     * @param model
     * @return
     */
    @PostMapping("/desasignarServicio")
    public String desasignarServicio(@RequestParam(name = "servicioDesasignado") String codigo,
            Model model) {
        servicioRealizadoDAO.desasignarServicio(codigo);
        return "bajaExitosa";
    }

}
